import React from "react";
const Home = React.lazy(() => import("./views/Pages/Home/index"));
const Map = React.lazy(() => import("./views/Pages/Map/index"));
const Dashboard = React.lazy(() => import("./views/Dashboard"));
const Users = React.lazy(() => import("./views/Users/Users"));
const User = React.lazy(() => import("./views/Users/User"));
const Price = React.lazy(() => import("./views/Pages/Price"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/home", exact: false, name: "Home", component: Home },
  { path: "/map", exact: true, name: "Map", component: Map },
  { path: "/price", exact: false, name: "Price", component: Price },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/users", exact: true, name: "Users", component: Users },
  { path: "/users/:id", exact: true, name: "User Details", component: User }
];

export default routes;
