//import apiLogger from '../api/Logger.js
import Cookies from 'universal-cookie';

const Logger = {
    // module: Hành động tại mô đun nào.
    // operation: Thao tác người dùng
    // description: Mô tả thao tác người dùng
    // user: Người dùng thực hiện thao tác
    // ip: ip người dùng
    // time: Thời gian hành động xảy ra
    saveHistory: function (module, operation, description) {
        if (!module || !operation || !description) return false;
        // kiểm tra người dùng
        const cookies = new Cookies();
        const userid= cookies.get('userid');
        if (!time) {
            let localeDate = new Date().toLocaleDateString().split("/");
            let localeTime = new Date().toLocaleTimeString();
            time = (localeDate[0] > 9 ? localeDate[0].toString() : '0' + localeDate[0]) + "/" +
                   (localeDate[1] > 9 ? localeDate[1].toString() : '0' + localeDate[1]) + "/" +
                   localeDate[2] + " " + localeTime;
        }
        let objHistory = {
            "module": module,
            "operation": operation,
            "description": description,
            "user": userid,
            "ip": ip,
            "time": time,
        }
        console.log(objHistory);
        // gọi apiLogger lưu lịch sử
    }
}

export default Logger;
