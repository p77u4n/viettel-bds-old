const area = [
    {id: 1, name: 'Paris', house: '10100', apartment: '9800', hire: '30', type: 1},
    {id: 2, name: 'Lyon', house: '5500', apartment: '4800', hire: '20', type: 1},
    {id: 3, name: 'Nantes', house: '3918', apartment: '3313', hire: '15', type: 1},
    {id: 4, name: 'Marseille', house: '4218', apartment: '3700', hire: '17', type: 1},
    {id: 5, name: 'Toulouse', house: '5415', apartment: '4700', hire: '19', type: 1},
    {id: 6, name: 'Ain', house: '10100', apartment: '9800', hire: '30', type: 2},
    {id: 7, name: 'Aisne', house: '5500', apartment: '4800', hire: '20', type: 2},
    {id: 8, name: 'Gers', house: '3918', apartment: '3313', hire: '15', type: 2},
    {id: 9, name: 'Eure', house: '4218', apartment: '3700', hire: '17', type: 2},
    {id: 10, name: 'Gard', house: '5415', apartment: '4700', hire: '19', type: 2},
];

export {area};