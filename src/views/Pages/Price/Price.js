import React, { useEffect } from 'react';
import './Price.css';
import { Row, Col, Input, Button, ButtonGroup, InputGroup, InputGroupAddon, Table, Card, CardHeader, CardBody, CardFooter, Modal, ModalBody, ModalHeader } from 'reactstrap';
import Map from './../Map';
import { Switch, Route, Link, useLocation } from 'react-router-dom';
import PriceDetail from './PriceDetail';
import PriceDetailWard from './PriceDetailWard';
import mapApi from './../../../api/map.api';
// import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
import SplitterLayout from "../components/SplitLayout/SplitterLayout";
import PerfectScrollbar from 'react-perfect-scrollbar';
var places = require('places.js');

const TOKEN = "803840e9bacbc2";

const datasetDefault = {
    label: 'Price',
    backgroundColor: 'rgba(75,192,192,0.2)',
    borderColor: '#63c2de',
    pointHoverBackgroundColor: '#fff',
    borderWidth: 2,
};
class Price extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            areaId: '',
            areaType: '',
            bdsType: 'house',
            update: false,
            showChart: false,
            showModal: false,
            location: {},
            data: [],
            datatable: [],
            tableOf: 'house',
            showColLeft: true,
            textSearch: '',
        }

        this.mapRef = React.createRef();
        // this.onCityChange = this.onCityChange.bind(this);
        // this.onDistrictChange = this.onDistrictChange.bind(this);
        // this.onWardChange = this.onWardChange.bind(this);
    }
    onCityChange = (obj) => {
        console.log('onCity change:' + obj.id);
        this.props.history.push('/price');
    }

    onDistrictChange = (obj) => {
        console.log('ond district change:' + obj.id);
        this.props.history.push('/price/district/' + obj.id);
        this.setState({ areaType: 'district', areaId: obj.id });

    }

    onWardChange = (obj) => {
        console.log("onWard change:", obj.id)
        this.props.history.push('/price/ward/' + obj.id);
        this.setState({ areaType: 'ward', areaId: obj.id });

    }

    changeBdsType(val) {
        this.loadTable(val);
    }
    loadTable(e) {
        if (e === 'house') {
            mapApi.getDistrictHouseStyle('VNM.27_1').then(data => {
                console.log('data: ', data);
                this.setState({ datatable: data, bdsType: e });
            })
        }
        else {
            mapApi.getDistrictAppartmentStyle('VNM.27_1').then(data => {
                console.log('data: ', data);
                this.setState({ datatable: data, bdsType: e });
            })
        }
    }
    // handleChange = (evt) => {
    //     this.setState({ textSearch: evt.target.value });
    // }

    // search = (e) => {
    //     e.preventDefault();
    //     let textSearch = this.state.textSearch;
    //     let me = this;
    //     if (textSearch.length > 2) {
    //         mapApi.algoliaGeocode(textSearch).then(function (result) {
    //             let loc = result._geoloc;
    //             // me.setState({ loc: loc });
    //             if (me.mapRef.current) {
    //                 me.mapRef.current.onLocationChange(loc);
    //                 me.setState({
    //                     showColLeft: true,
    //                 })
    //             }
    //         })
    //     }
    // }
    // onDragEnd = () => {
    //     if (this.mapRef.current) {
    //         // console.log("mapcurrent:",this.mapRef.current);
    //         this.mapRef.current.resizeMap();
    //     }
    // }
    // toggleSidebar = () => {
    //     this.setState({ showColLeft: !this.state.showColLeft }, () => {
    //         this.onDragEnd();
    //     });
    // }
    componentDidMount() {
        // var placesAutocomplete = places({
        //     appId: 'pl0K4C57LHC4',
        //     apiKey: '74d8852573f9c6ff69b6e9e87f05efb6',
        //     container: document.querySelector('#address-input')
        // });

        // placesAutocomplete.configure({
        //     language: 'vn',
        //     countries: ['vn'],
        // });

        // placesAutocomplete.on('change', e => {
        //     console.log(e.suggestion);
        //     this.mapRef.current.onLocationChange(e.suggestion.latlng);
        // });

        // let me = this;
        // mapApi.getDistrictHouseStyle('VNM.27_1').then(data => {
        //     console.log('data: ', data);
        //     this.setState({ datatable: data });
        // })
        // me.onRadioBtnClick(1);

        // fetch('https://ipinfo.io/?token=' + TOKEN)
        //     .then(function (response) {
        //         return response.json();
        //     })
        //     .then(function (data) {
        //         console.log(data);
        //         me.setState({ location: data });
        //     });

    }
    render() {
        return (
            <div>
                <div className="price-col-left-draft" style={{ height: '100%' }}>
                    <PerfectScrollbar>
                        <Switch>
                            <Route exact path="/price">
                                <div>
                                    <div>
                                        <Card>
                                            <CardHeader><strong>Bảng giá bất động sản</strong></CardHeader>
                                            <CardBody>
                                                <Table>
                                                    <thead>
                                                        <tr>
                                                            <th>Khu vực</th>
                                                            <th>Giá thấp nhất</th>
                                                            <th>Giá cao nhất</th>
                                                            <th>Giá trung bình</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {this.state.datatable.map(i => (
                                                            <tr key={i.id}>
                                                                <td><Link to={'/price/district/' + i.id}>{i.name}</Link></td>
                                                                <td>{i.min.toFixed(1)}</td>
                                                                <td>{i.max.toFixed(1)}</td>
                                                                <td>{i.ppm2_mean.toFixed(1)}</td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </Table>
                                            </CardBody>
                                        </Card>
                                    </div>
                                </div>
                            </Route>
                            <Route path="/price/district/:id/">
                                <PriceDetail {...this.props}
                                    id={this.state.areaId}
                                    type={this.state.areaType}
                                    bdsType={this.state.bdsType}
                                />
                            </Route>
                            <Route path="/price/ward/:id/">
                                <PriceDetailWard {...this.props}
                                    id={this.state.areaId}
                                    type={this.state.areaType}
                                    bdsType={this.state.bdsType}
                                />
                            </Route>

                        </Switch>
                        <div className='toggleview'>
                            <footer className="footer">
                                <div className="footer__content">
                                    <div className="footer__menu">
                                        <div className="footer__menu-header">Giới thiệu</div>
                                        <a className="footer__menu-item" href="/a-propos/terms/">Tại sao MeilleurAgents?</a>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Báo chí nói gì về chúng tôi?</a>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Thông tin tuyển dụng</a>
                                    </div>

                                    <div className="footer__menu">
                                        <div className="footer__menu-header">Điều kiện</div>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Điều kiện sử dụng chung</a>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Thông báo pháp lý</a>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Phí bán hàng của chúng tôi</a>
                                        <a className="footer__menu-item" href="/a-propos/espace-presse/">Chính sách bảo mật</a>
                                    </div>

                                    <div className="footer__menu">
                                        <div className="footer__menu-header">Trợ giúp</div>
                                        <a className="footer__menu-item" href="/support/faq/">Câu hỏi thường gặp (FAQ)</a>
                                        <a className="footer__menu-item" href="/contact/">Liên hệ với chúng tôi</a>
                                    </div>

                                    <div className="footer__lastline">
                                        <div>
                                            <div className="footer__happyAtWork-link">
                                                <img src="https://www.meilleursagents.com/static/images/happy_at_work_2018.jpg?t=1585642831" alt="Số 1 năm 2018 theo khảo sát hạnh phúc tại nơi làm việc" />
                                            </div>
                                        </div><font><font >
                                            © BestAgents. </font>
                                            <font >Bản quyền 2008 - 2020.</font>
                                        </font></div></div>
                            </footer>
                        </div>
                    </PerfectScrollbar>
                </div>
            </div>
        )
    }
}

export default Price;