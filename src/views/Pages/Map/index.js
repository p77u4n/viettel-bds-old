import React from "react";
import mapboxgl from "mapbox-gl";
import AppConfig from "../../../AppConfig";
import api from "./../../../api/map.api"
import polygonCenter from "geojson-polygon-center"
import geojsonExtent from 'geojson-extent'
import { MapboxStyleSwitcherControl } from "mapbox-gl-style-switcher";
import "mapbox-gl-style-switcher/styles.css";
import _ from "lodash"
import houseApi from "./../../../api/house.api"
import apartmentApi from "./../../../api/appartment.api"
const colorRange = ['#085', '#13be00', '#75e100', '#aee500', '#dfff00', '#fff831', '#ffd500', '#ffa51f', '#ff7b16', '#ff0a02', '#c80000']
// const colorRange1 = ["#5ecf91", "#63c62a", "#8eca39", "#b5da32", "#e2f32f", "#fefd3e", "#fbf33e", "#fadb42", "#fcca3c", "#f8b53d", "#fca645", "#fc9545", "#ef794f", "#f2564c", "#e64c85", "#d147a4"]
const colorRange1 = ['#00e691', '#085', '#5eff4d', '#13be00', '#c1ff80', '#75e100', '#84ff00', '#4f9900', '#e7ff99', '#aee500', '#759900', '#f1ff99', '#e7ff4d', '#dfff00', '#fff831', '#999400', '#ffe14d', '#ffd500', '#ffe0b3', '#ffa51f', '#ffe2cc', '#ffa866', '#ff9a4d', '#ff7b16', '#cc5800', '#ffb5b3', '#ff8480', '#ff0a02', '#ff4d4d', '#c80000']
mapboxgl.accessToken = "pk.eyJ1IjoiY2hpbmhscyIsImEiOiIwNFhac19NIn0.ywzcihERU9ETEyUxphFcIQ";
class MapComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      map: null,
      popup: null,
      selectedId: null,
      minValue: null,
      colors: [],
      showOverLay: false,
      maxValue: null,
      name: "",
      ppm2_mean: null,
      minPrice: null,
      maxPrice: null,
      type: this.props.type,
      dhouseObj: null,
      dapparmentObj: null,
      whouseObj: null,
      wapparmentObj: null,
      firstLoad: false,
      house: [],
      appartments: [],
      features: [],
      selectedSoure: null,
      selectedFeature: null,
      selectedTinhId: null,
      selectedHuyenId: null,
      selectedXaId: null,
      hoveredStateId: null,
      tinhEvent: 0,
      huyenEvent: 0,
      xaEvent: 0,
      pointEvent: 0,
      lng: 105,
      lat: 21,
      zoom: 5,
      cityObj: {}
    };
  }

  componentDidMount() {
    this.initMap();
  }
  onLocationChange = (loc) => {
    let map = this.state.map;
    if (map && loc) {
      map.flyTo({
        center: [loc.lng, loc.lat],
        zoom: 8.5,
        essential: true // this animation is considered essential with respect to prefers-reduced-motion
      });
      if (this.state.marker) {
        this.state.marker.remove();
      }
      var marker = new mapboxgl.Marker().setLngLat([loc.lng, loc.lat]).addTo(map);
      this.setState({ marker: marker })
      this.hilightSelectedFeature(loc);
    }
  }
  hilightSelectedFeature = (loc) => {
    let map = this.state.map;
    var myInterVal = setInterval(() => {
      if (map.isSourceLoaded('diaphanhuyen') && map.isSourceLoaded("diaphanxa")) {
        clearInterval(myInterVal);
        var point = map.project([loc.lng, loc.lat]);
        var features = map.queryRenderedFeatures(point, { layers: ['tinh', 'huyen', 'xa'] });
        var xaFea = null;
        var huyenFea = null;
        var tinhFea = null;
        if (features && features.length > 0) {
          for (var i = 0; i < features.length; i++) {
            if (features[i].layer.id === "xa") {
              xaFea = features[i];
              break;
            }
            else if (features[i].layer.id === "huyen") {
              huyenFea = features[i];
            }
            else if (features[i].layer.id === "tinh") {
              tinhFea = features[i];
            }
          }
          if (xaFea) {
            var wid = xaFea.properties.ward_id;
            map.setFilter("selectXa", ["==", "ward_id", wid]);
          } else {
            if (huyenFea) {
              var did = huyenFea.properties.district_id;
              map.setFilter("selectHuyen", ["==", "district_id", did]);
            } else {
              if (tinhFea) {
                var cid = tinhFea.properties.district_id;
                this.setState({ selectedTinhId: cid, selectedFeature: tinhFea })
                this.styleHuyenFeatures(tinhFea, true)
              }
            }
          }
        }
      }
    }, 1000);
  }
  clearAdmin() {
    let map = this.state.map;
    let marker = this.state.marker;
    if (marker) marker.remove();
    // var geodata = { 'type': 'FeatureCollection', 'features': [] }
    // map.getSource('admin').setData(geodata);
  }
  onLocatePoint = (item) => {
    let map = this.state.map;
    if (map && item) {
      if (map.getZoom() < 11 )
        {
          map.flyTo({
            center: [item.lng, item.lat],
            zoom: 13.5,// zoom hien thi neu dang o muc xa
            essential: true // this animation is considered essential with respect to prefers-reduced-motion
          });
        }
      else {
        map.flyTo({
          center: [item.lng, item.lat],
          zoom: map.getZoom(),// di chuyen toi location cung muc zoom
          essential: true // this animation is considered essential with respect to prefers-reduced-motion
        });
      }
      
      if (this.state.marker) {
        this.state.marker.remove();
      }
      var marker = new mapboxgl.Marker().setLngLat([item.lng, item.lat]).addTo(map);
      this.setState({ marker: marker });
      this.hilightSelectedFeature(item);
    }
  }
  changeMapStyle() {

  }
  resizeMap = () => {
    let map = this.state.map;
    if (map) {
      map.resize();
    }
  }
  onHomValueChange = (type) => {
    let me = this;
    this.setState({ type: type });
    if (this.state.selectedFeature) {
      if (this.state.selectedFeature.layer.id === 'xa') {
        if (type === 'sale_house')
          this.renderPointFeatures(me.state.house);
        else this.renderPointFeatures(me.state.appartments)
      }
      else if (this.state.selectedFeature.layer.id === 'huyen') {
        this.styleXaFeatures(this.state.selectedFeature, true);
      } else {
        this.styleHuyenFeatures(this.state.selectedFeature, true);
      }
    }
  }
  initMap = () => {
    let me = this;
    let map = new mapboxgl.Map({
      container: this.mapContainer,
      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
      center: [this.state.lng, this.state.lat],
      zoom: this.state.zoom
    });


    function renderMap() {
      map.addSource("diaphantinh", {
        type: "vector",
        tiles: [AppConfig.MAPAPI + "/diaphan/tinh/tile/{z}/{x}/{y}"]
      });
      map.addSource("diaphanhuyen", {
        type: "vector",
        tiles: [AppConfig.MAPAPI + "/diaphan/huyen/tile/{z}/{x}/{y}"]
      });
      map.addSource("diaphanxa", {
        type: "vector",
        tiles: [AppConfig.MAPAPI + "/diaphan/xa/tile/{z}/{x}/{y}"]
      });
      map.addSource("chuyen", {
        'type': 'geojson',
        'data': {
          'type': 'FeatureCollection',
          'features': []
        }
      });
      map.addSource("cxa", {
        'type': 'geojson',
        'data': {
          'type': 'FeatureCollection',
          'features': []
        }
      });
      map.addSource("admin", {
        'type': 'geojson',
        'data': {
          'type': 'FeatureCollection',
          'features': []
        }
      });
      map.addSource("points", {
        'type': 'geojson',
        'data': {
          'type': 'FeatureCollection',
          'features': []
        },
        cluster: true,
        clusterMaxZoom: 15, // Max zoom to cluster points on
        clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)

      });

      map.addLayer({
        id: "tinh",
        type: "fill",
        source: "diaphantinh",
        "source-layer": "tinh",
        "maxzoom": 10.5,
        paint: {
          "fill-outline-color": "#00cc66",
          "fill-color": "transparent",
          "fill-opacity": 1
        }
      });
      map.addLayer({
        id: "huyen",
        type: "fill",
        source: "diaphanhuyen",
        "source-layer": "huyen",
        "minzoom": 8.5,
        "maxzoom": 13.5,
        paint: {
          'fill-outline-color': "transparent",
          "fill-color": "transparent",
          "fill-opacity": 0
        }
      });
      map.addLayer({
        id: "chuyen",
        type: "fill",
        source: "chuyen",
        "minzoom": 8.5,
        "maxzoom": 11.4,
        paint: {
          'fill-outline-color': "red",
          'fill-color': 'white',
          "fill-opacity": 0.5
        }
      });

      map.addLayer({
        id: "xa",
        type: "fill",
        source: "diaphanxa",
        "minzoom": 11.5,
        "source-layer": "xa",
        paint: {
          "fill-outline-color": "green",
          "fill-color": "transparent",
          "fill-opacity": 1
        },
        filter: ["==", 'ward_id', ""]
      });
      map.addLayer({
        id: "cxa",
        type: "fill",
        source: "cxa",
        "minzoom": 11.5,
        "maxzoom": 13.5,
        paint: {
          'fill-outline-color': "white",
          'fill-color': {
            property: 'ppm2_mean',
            stops: [[0, 'white'], [10, 'green'], [30, 'blue'], [50, 'yellow'], [80, 'red']]
          },
          "fill-opacity": 0.6
        }
      });
      // map.addLayer({
      //   id: "admin",
      //   type: "fill",
      //   source: "admin",
      //   "minzoom": 6,
      //   "maxzoom": 13,
      //   paint: {
      //     'fill-outline-color': "red",
      //     'fill-color': 'white',
      //     "fill-opacity": 0.7
      //   }
      // });
      map.addLayer(
        {
          id: "highlightTinh",
          type: "line",
          source: "diaphantinh",
          "source-layer": "tinh",
          paint: {
            "line-width": 1.5,
            "line-color": '#1e91ff'
          },
          filter: ["==", "city_id", ""]
        }
      );
      map.addLayer(
        {
          id: "highlightHuyen",
          type: "line",
          source: "diaphanhuyen",
          "source-layer": "huyen",
          paint: {
            "line-width": 1.5,
            "line-color": '#1e91ff'
          },
          filter: ["==", "district_id", ""]
        }
      );
      map.addLayer(
        {
          id: "highlightXa",
          type: "line",
          source: "diaphanxa",
          "source-layer": "xa",
          paint: {
            "line-width": 2,
            "line-color": '#1e91ff'
          },
          filter: ["==", "ward_id", ""]
        }
      );

      map.addLayer(
        {
          id: "selectTinh",
          type: "line",
          source: "diaphantinh",
          "source-layer": "tinh",
          paint: {
            "line-width": 2,
            "line-color": 'red'
          },
          filter: ["==", "city_id", ""]
        }
      );
      map.addLayer(
        {
          id: "selectHuyen",
          type: "line",
          source: "diaphanhuyen",
          "source-layer": "huyen",
          "minzoom": 8.5,
          "maxzoom": 12,
          paint: {
            "line-width": 1.5,
            "line-color": 'blue'
          },
          filter: ["==", "district_id", ""]
        }
      );
      map.addLayer(
        {
          id: "selectXa",
          type: "line",
          source: "diaphanxa",
          "source-layer": "xa",
          "minzoom": 12,
          paint: {
            "line-width": 1.5,
            "line-color": 'blue'
          },
          filter: ["==", "ward_id", ""]
        }
      );
      map.addLayer({
        'id': 'points',
        'type': 'circle',
        'source': 'points',
        "minzoom": 13.5,
        "paint": {
          "circle-radius": 5,
          "circle-color": [
            'case',
            ['boolean', ['feature-state', 'hover'], false],
            'blue',
            '#1e91ff'
          ],
          "circle-stroke-color": 'white',
          "circle-stroke-width": 2
        }
      });
      map.addLayer({
        'id': 'cluster-point',
        'type': 'circle',
        'source': 'points',
        "minzoom": 13.5,
        filter: ['has', 'point_count'],
        "paint": {
          'circle-color': [
            'step',
            ['get', 'point_count'],
            '#51bbd6',
            100,
            '#f1f075',
            750,
            '#f28cb1'
          ],
          'circle-radius': [
            'step',
            ['get', 'point_count'],
            10,
            50,
            15,
            300,
            20
          ],
          "circle-stroke-color": 'white',
          "circle-stroke-width": 2
        }
      });
      map.addLayer({
        id: 'cluster-count',
        type: 'symbol',
        source: 'points',
        "minzoom": 13.5,
        filter: ['has', 'point_count'],
        layout: {
          'text-field': '{point_count_abbreviated}',
          'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
          'text-size': 12
        }
      });
      me.toggleLayerXaEvent(1);
      me.toggleLayerHuyenEvent(1);
      me.toggleLayerTinhEvent(1);
      me.toggleLayerPointEvent(1);
      // me.setState({ tinhEvent: 1, popup: popup });
      map.resize();
    }

    map.on('style.load', function (e) {
      renderMap();
    });

    map.on('zoomend', (evt) => {
      const currentZoom = map.getZoom();
      if (currentZoom >= 13.5) {//hien thi point
        var myInterVal = setInterval(() => {
          if (map.isSourceLoaded('diaphanxa')) {
            clearInterval(myInterVal)
            var center = map.getCenter();
            var point = map.project([center.lng, center.lat]);
            var features = map.queryRenderedFeatures(point, { layers: ['xa'] });
            if (me.state.selectedXaId === null) {
              for (var i = 0; i < features.length; i++) {
                me.setState({ selectedFeature: features[i], selectedXaId: features[i].ward_id })
                me.onXaSelectFeature(features[i]);
                break;
              }
            } else {
              let fea = null;
              let isMe = false;
              for (var i = 0; i < features.length; i++) {
                fea = features[i];
                if (me.state.selectedXaId === fea.properties.ward_id) {
                  isMe = true;
                  break;
                }
              }
              if (!isMe) {
                if (fea) {
                  me.setState({ selectedFeature: fea, selectedXaId: fea.properties.ward_id })
                  me.onXaSelectFeature(fea);
                }
              }
            }
          }
        }, 1000)
      }
      else if (currentZoom >= 11.5 && currentZoom < 13.5) {//xa
        var myInterVal = setInterval(() => {
          if (map.isSourceLoaded('diaphanhuyen')) {
            clearInterval(myInterVal)
            var center = map.getCenter();
            var point = map.project([center.lng, center.lat]);
            var features = map.queryRenderedFeatures(point, { layers: ['huyen'] });
            if (me.state.selectedHuyenId === null) {
              for (var i = 0; i < features.length; i++) {
                me.setState({ selectedFeature: features[i], selectedHuyenId: features[i].district_id })
                me.onHuyenSelectFeature(features[i]);
                break;
              }
            } else {
              let fea = null;
              let isMe = false;
              for (var i = 0; i < features.length; i++) {
                fea = features[i];
                if (me.state.selectedHuyenId === fea.properties.district_id) {
                  isMe = true;
                  break;
                }
              }
              if (!isMe) {
                if (fea) {
                  me.setState({ selectedFeature: fea, selectedHuyenId: fea.properties.district_id })
                  me.onHuyenSelectFeature(fea);
                }
              }
            }
          }
        }, 1000);
      }
      else if (currentZoom >= 8.5 && currentZoom < 11.5) {//huyen
        me.setState({ showOverLay: true });
        map.setFilter("selectXa", ["==", "ward_id", ""]);
        if (!me.state.selectedTinhId) {
          var myInterVal = setInterval(() => {
            if (map.isSourceLoaded('diaphantinh')) {
              clearInterval(myInterVal);
              var center = map.getCenter();
              var point = map.project([center.lng, center.lat]);
              var features = map.queryRenderedFeatures(point, { layers: ['tinh'] });
              if (features.length > 0) {
                for (var i = 0; i < features.length; i++) {
                  me.onTinhSelectFeature(features[i]);
                  me.setState({ selectedFeature: features[i], selectedTinhId: features[i].properties.city_id })
                  break;
                }
              }
            }
          }, 1000);
        } else {
          me.props.cityChange(me.state.cityObj);
        }
      }
      else {//tinh
        map.setFilter("selectHuyen", ["==", "district_id", ""]);
        if (me.state.selectedTinhId === null) {
          map.on('sourcedata', function (e) {
            if (map.isSourceLoaded('diaphantinh') && e.sourceId === 'diaphantinh') {
              var center = map.getCenter();
              var point = map.project([center.lng, center.lat]);
              var features = map.queryRenderedFeatures(point, { layers: ['tinh'] });
              if (features.length > 0) {
                for (var i = 0; i < features.length; i++) {
                  me.onTinhSelectFeature(features[i]);
                  me.setState({ selectedFeature: features[i], selectedTinhId: features[i].properties.city_id })
                  // me.props.cityChange({ id: me.state.selectedTinhId, name: features[i].properties.city_name });
                  me.props.cityChange(me.state.cityObj);
                  break;
                }
              }
            }
          })
        } else {
          // me.props.cityChange({ id: me.state.selectedTinhId, name: me.state.selectedFeature.properties.city_name });
          me.props.cityChange(me.state.cityObj);
        }
        if (this.state.huyenEvent === 1) {
          this.setState({ huyenEvent: 0 });
        }
        me.setState({ showOverLay: false });
      }
    });


    map.addControl(new mapboxgl.NavigationControl());
    map.addControl(
      new mapboxgl.GeolocateControl({
        positionOptions: {
          enableHighAccuracy: true
        },
        trackUserLocation: true
      })
    );
    const styles = [
      {
        title: "Nền tối màu",
        uri: "mapbox://styles/mapbox/dark-v9"
      },
      {
        title: "Nền sáng màu",
        uri: "mapbox://styles/mapbox/light-v9"
      },
      {
        title: "Nền giao thông",
        uri: "mapbox://styles/mapbox/streets-v11"
      },
      {
        title: "Nền vệ tinh",
        uri: "mapbox://styles/mapbox/satellite-v9",
      }

    ];
    map.addControl(new MapboxStyleSwitcherControl(styles));
    var popup = new mapboxgl.Popup({
      closeButton: true,
      closeOnClick: true
    });


    me.setState({ map: map, popup: popup });
  }
  toggleLayerTinhEvent = (status) => {
    var map = this.state.map;
    if (map)
      if (status === 0) {
        map.off("mousemove", "tinh", this.onTinhMouseMove);
        map.off("dblclick", "tinh", this.onTinhMouseDown);
        map.off("click", "tinh", this.onTinhSelect);
        map.off("mouseleave", "tinh", this.onTinhMouseLeave);
      } else {
        map.on("mousemove", "tinh", this.onTinhMouseMove);
        map.on("dblclick", "tinh", this.onTinhMouseDown);
        map.on("click", "tinh", this.onTinhSelect);
        map.on("mouseleave", "tinh", this.onTinhMouseLeave);
      }

  }


  toggleLayerHuyenEvent = (status) => {
    var map = this.state.map;
    if (map)
      if (status === 0) {
        map.off("click", "huyen", this.onHuyenSelect);
        map.off("mousemove", "huyen", this.onHuyenMouseMove);
        map.off("dblclick", "huyen", this.onHuyenMouseDown);
        map.off("mouseleave", "huyen", this.onHuyenMouseLeave);
      } else {
        map.on("click", "huyen", this.onHuyenSelect);
        map.on("mousemove", "huyen", this.onHuyenMouseMove);
        map.on("dblclick", "huyen", this.onHuyenMouseDown);
        map.on("mouseleave", "huyen", this.onHuyenMouseLeave);
      }
  }

  toggleLayerXaEvent = (status) => {
    var map = this.state.map;
    if (map)
      if (status === 0) {
        map.off("mousemove", "xa", this.onXaMouseMove);
        map.off("click", "xa", this.onXaSelect);
        map.off("dblclick", "xa", this.onXaMouseDown);

        map.off("mouseleave", "xa", this.onXaMouseLeave);
      } else {
        map.on("click", "xa", this.onXaSelect);
        map.on("mousemove", "xa", this.onXaMouseMove);
        map.on("dblclick", "xa", this.onXaMouseDown);
        map.on("mouseleave", "xa", this.onXaMouseLeave);
      }
  }
  toggleLayerPointEvent = (status) => {
    var map = this.state.map;
    map.on("mousemove", "points", this.onPointsMouseMove);
    map.on("mouseleave", "points", this.onPointsMouseLeave);
    // map.on("dblclick", "points", function () {

    // });
  }
  onTinhSelect = (e) => {
    let me = this;
    e.preventDefault();
    if (me.state.huyenEvent !== 1) {
      var map = this.state.map;
      var feature = e.features[0];
      // this.onTinhSelectFeature(feature);
    }
  }
  onTinhMouseDown = (e) => {
    let me = this;
    e.preventDefault();
    if (me.state.huyenEvent !== 1) {
      var map = this.state.map;
      var feature = e.features[0];
      this.onTinhSelectFeature(feature);
    }
  }
  onTinhSelectFeature = (feature, level) => {
    let me = this;
    var map = this.state.map;
    map.setFilter("huyen", ["==", "city_id", feature.properties.city_id]);
    map.setFilter("chuyen", ["==", "city_id", feature.properties.city_id]);
    setTimeout(() => {
      var center = polygonCenter(feature.geometry);
      if (level)
        map.flyTo({ center: center.coordinates, zoom: 8.5 });
    }, 500);//cho resize left panel 

    if (!me.state.selectedId || me.state.selectedId != feature.properties.city_id) {
      me.setState({ selectedId: feature.properties.city_id, selectedFeature: feature });
      me.styleHuyenFeatures(feature, true);
    }
  }
  renderHuyenStyle = () => {
    let me = this;
    var map = this.state.map;
    var stops, dmap;
    if (me.state.type === 'sale_house') {
      stops = me.state.dhouseObj.stops;
      dmap = me.state.dhouseObj.dmap;
    } else {
      stops = me.state.dapparmentObj.stops;
      dmap = me.state.dapparmentObj.dmap;
    }
    var features = me.state.features;
    for (var i = 0; i < features.length; i++) {
      if (dmap[features[i].properties.district_id]) {
        features[i].properties.ppm2_mean = dmap[features[i].properties.district_id].ppm2_mean;
      } else
        features[i].properties.ppm2_mean = 0;
    }
    var newFillColor = { property: 'ppm2_mean', stops: stops }
    let colors = [];
    for (var i = 0; i < stops.length - 1; i++) {
      colors.push({
        from: stops[i][0].toFixed(2),
        to: stops[i + 1][0].toFixed(2),
        color: stops[i][1]
      })
    }
    me.setState({ minValue: stops[0][0].toFixed(2), colors: colors, maxValue: stops[stops.length - 1][0].toFixed(2) })
    map.setPaintProperty('chuyen', 'fill-color', newFillColor);
    var geodata = { 'type': 'FeatureCollection', 'features': features }
    map.getSource('chuyen').setData(geodata);
  }
  styleHuyenFeatures = (feature, trigger) => {
    let me = this;
    var map = this.state.map;
    var promises = [];
    var styleHouseRequest = new Promise((resolve, reject) => {
      api.getDistrictHouseStyle(feature.properties.city_id).then(function (response) {
        if (response && response.length) {
          resolve(response)
        } else
          resolve([]);
      })
    });
    promises.push(styleHouseRequest);
    var styleAppartmentRequest = new Promise((resolve, reject) => {
      api.getDistrictAppartmentStyle(feature.properties.city_id).then(function (response) {
        if (response && response.length) {
          resolve(response)
        } else
          resolve([]);
      })
    })
    promises.push(styleAppartmentRequest);
    var featureRequest = new Promise((resolve, reject) => {
      api.getFeaturesById("huyen", feature.properties.city_id).then(function (response) {
        if (response.status == 200) {
          var features = response.data;
          resolve(features)
        } else
          resolve([]);
      })
    });
    promises.push(featureRequest);
    Promise.all(promises).then(values => {
      var stylesHouse = values[0];
      var stylesApparment = values[1];
      if (stylesHouse.length == 0 && stylesApparment == 0) return false;
      var features = values[2];
      var houseObj = me.getStyleObject(stylesHouse);
      var apparmentObj = me.getStyleObject(stylesApparment);
      me.setState({ dhouseObj: houseObj, dapparmentObj: apparmentObj, features: features, showOverLay: true })
      me.renderHuyenStyle();
      if (trigger && me.props.cityChange) {
        let id = feature.properties.city_id;
        let name = feature.properties.city_type + ',' + feature.properties.city_name;
        let changeObj = {
          id: id,
          name: name,
          fromValueCC: apparmentObj.fromValue,
          toValueCC: apparmentObj.toValue,
          totalMeanCC: apparmentObj.total_mean,
          fromValueND: houseObj.fromValue,
          toValueND: houseObj.toValue,
          totalMeanND: houseObj.total_mean
        }
        me.setState({ cityObj: changeObj });
        me.props.cityChange(changeObj);
      }
    })
  }
  getStyleObject = (styles) => {
    var dmap = {};
    var distinctStyles = _.uniqBy(styles, function (e) { return e.ppm2_mean; });
    var orderStyle = _.orderBy(distinctStyles, ['ppm2_mean'], ['asc']);
    for (var i = 0; i < styles.length; i++) {
      dmap[styles[i].id] = styles[i];
    }
    let fromValue = orderStyle[0].ppm2_mean;
    let toValue = orderStyle[orderStyle.length - 1].ppm2_mean;
    let total_mean = 0;
    let stops = [];
    let colorStops;
    if (orderStyle.length <= colorRange.length) colorStops = colorRange;
    // else if (orderStyle.length <= colorRange1.length) colorStops = colorRange1;
    else colorStops = colorRange1;
    for (var i = 0; i < orderStyle.length; i++) {
      total_mean += orderStyle[i].ppm2_mean;
      stops.push([parseFloat(orderStyle[i].ppm2_mean.toFixed(2)), colorStops[i]]);
    }
    total_mean = total_mean / orderStyle.length;
    return {
      dmap: dmap,
      fromValue: fromValue.toFixed(2),
      toValue: toValue.toFixed(2),
      total_mean: total_mean.toFixed(2),
      stops: stops
    }
  }
  onTinhMouseMove = (e) => {
    let me = this;
    var map = this.state.map;
    var feature = e.features[0];
    map.setFilter("highlightTinh", ["==", "city_id", feature.properties.city_id]);
    if (document.getElementById('txtAddress')) document.getElementById('txtAddress').innerText = feature.properties.city_name;
  }
  onTinhMouseLeave = (e) => {
    var map = this.state.map;
    map.setFilter("highlightTinh", ["==", "city_id", '']);
  }
  onHuyenSelect = (e) => {
    let me = this;
    var map = this.state.map;
    me.setState({ huyenEvent: 1 });
    setTimeout(() => {
      me.setState({ huyenEvent: 0 });//bo su kien de co the click vao tinh
    }, 3000);
    e.preventDefault();
    if (me.state.xaEvent !== 1) {
      var features = map.queryRenderedFeatures(e.point, { layers: ['huyen'] });
      if (features && features.length > 0) {
        var feature = features[0];
        if (!me.state.selectedId || me.state.selectedId != feature.properties.district_id) {
          me.setState({ selectedId: feature.properties.district_id, selectedFeature: feature, features: features })
          map.setFilter("selectHuyen", ["==", "district_id", feature.properties.district_id]);
          map.setFilter("selectXa", ["==", "city_id", ""]);
        }
      }
    }
  }
  onHuyenMouseDown = (e) => {
    let me = this;
    var map = this.state.map;
    me.setState({ huyenEvent: 1 });
    setTimeout(() => {
      me.setState({ huyenEvent: 0 });//bo su kien de co the click vao tinh
    }, 3000);
    e.preventDefault();
    if (me.state.xaEvent !== 1) {
      var features = map.queryRenderedFeatures(e.point, { layers: ['huyen'] });
      if (features && features.length > 0) {
        for (var i = 0; i < features.length; i++) {
          var feature = features[i];
          if (!me.state.selectedId || me.state.selectedId != feature.properties.district_id) {
            me.setState({ selectedId: feature.properties.district_id, selectedFeature: feature, features: features })
          }
          this.onHuyenSelectFeature(feature, true);
          break;

        }
      }
    }
  }
  onHuyenSelectFeature = (feature, level) => {
    let me = this;
    var map = this.state.map;
    me.setState({ huyenEvent: 1 });
    map.setFilter("xa", ["==", "district_id", feature.properties.district_id]);
    map.setFilter("cxa", ["==", "district_id", feature.properties.district_id]);
    me.styleXaFeatures(feature, true);
    var center = polygonCenter(feature.geometry);
    if (level)
      map.flyTo({ center: center.coordinates, zoom: 11.5 });
  }

  renderXaStyle = () => {
    let me = this;
    var map = this.state.map;
    var stops, dmap;
    if (me.state.type === 'sale_house') {
      stops = me.state.whouseObj.stops;
      dmap = me.state.whouseObj.dmap;
    } else {
      stops = me.state.wapparmentObj.stops;
      dmap = me.state.wapparmentObj.dmap;
    }
    var features = me.state.features;
    for (var i = 0; i < features.length; i++) {
      if (dmap[features[i].properties.ward_id]) {
        features[i].properties.ppm2_mean = dmap[features[i].properties.ward_id].ppm2_mean;
      } else
        features[i].properties.ppm2_mean = 0;
    }
    var newFillColor = { property: 'ppm2_mean', stops: stops }
    let colors = [];
    for (var i = 0; i < stops.length - 1; i++) {
      colors.push({
        from: stops[i][0].toFixed(2),
        to: stops[i + 1][0].toFixed(2),
        color: stops[i][1]
      })
    }
    me.setState({ minValue: stops[0][0].toFixed(2), colors: colors, maxValue: stops[stops.length - 1][0].toFixed(2) })
    map.setPaintProperty('cxa', 'fill-color', newFillColor);
    var geodata = { 'type': 'FeatureCollection', 'features': features }
    map.getSource('cxa').setData(geodata);
  }
  styleXaFeatures = (feature, trigger) => {
    let me = this;
    var map = this.state.map;
    var promises = [];
    var styleHouseRequest = new Promise((resolve, reject) => {
      api.getWardHouseStyle(feature.properties.district_id).then(function (response) {
        if (response && response.length) {
          resolve(response)
        } else
          resolve([]);
      })
    });
    promises.push(styleHouseRequest);
    var styleAppartmentRequest = new Promise((resolve, reject) => {
      api.getWardAppartmentStyle(feature.properties.district_id).then(function (response) {
        if (response && response.length) {
          resolve(response)
        } else
          resolve([]);
      })
    })
    promises.push(styleAppartmentRequest);
    var featureRequest = new Promise((resolve, reject) => {
      api.getFeaturesById("xa", feature.properties.district_id).then(function (response) {
        if (response.status == 200) {
          resolve(response.data)
        } else
          resolve([]);
      })
    });
    promises.push(featureRequest);
    Promise.all(promises).then(values => {
      var stylesHouse = values[0];
      var stylesApparment = values[1];
      if (stylesHouse.length === 0 && stylesApparment.length === 0) return false;
      var features = values[2];
      var houseObj = me.getStyleObject(stylesHouse);
      var apparmentObj = me.getStyleObject(stylesApparment);
      me.setState({ whouseObj: houseObj, wapparmentObj: apparmentObj, features: features })
      me.renderXaStyle();
      if (trigger && me.props.districtChange) {
        let id = feature.properties.district_id;
        let name = feature.properties.district_type + ',' + feature.properties.district_name;
        let changeObj = {
          id: id,
          name: name,
          fromValueCC: apparmentObj.fromValue,
          toValueCC: apparmentObj.toValue,
          totalMeanCC: apparmentObj.total_mean,
          fromValueND: houseObj.fromValue,
          toValueND: houseObj.toValue,
          totalMeanND: houseObj.total_mean
        }
        me.props.districtChange(changeObj);
      }
    });
  }
  onHuyenMouseMove = (e) => {
    var map = this.state.map;
    var popup = this.state.popup;
    let me = this;
    if (me.state.xaEvent !== 1) {
      var feature = e.features[0];
      var props = feature.properties;
      map.setFilter("highlightHuyen", ["==", "district_id", feature.properties.district_id]);
      if (document.getElementById('txtAddress')) document.getElementById('txtAddress').innerText = feature.properties.district_name;
      var dmap;
      if (me.state.type === 'sale_house') {
        if (me.state.dhouseObj)
          dmap = me.state.dhouseObj.dmap;
      } else {
        if (me.state.dapparmentObj)
          dmap = me.state.dapparmentObj.dmap;
      }
      if (dmap && dmap[props.district_id]) {
        var info = dmap[props.district_id];
        me.setState({ name: info.name, ppm2_mean: info.ppm2_mean.toFixed(2), minPrice: info.min.toFixed(2), maxPrice: info.max.toFixed(2) })
      }
    }
    // var center = polygonCenter(feature.geometry);
    //   var html = `<div>
    //     <ul style="list-style:none;">  
    //       <li><span style="font-weight:bold;">Tên: </span><span>`+ info.name + `</span></li>
    //       <li><span>Giá/m²: </span><span>`+ info.ppm2_mean.toFixed(2) + `</span></li>
    //       <li><span>Từ: </span><span>`+ info.min.toFixed(2) + `</span></li>
    //       <li><span>Đến: </span><span>`+ info.max.toFixed(2) + `</span></li>
    //     </ul>
    //   </div>`
    //   popup
    //     .setLngLat(center.coordinates)
    //     .setHTML(html)
    //     .addTo(map);
    // }
  }

  onHuyenMouseLeave = (e) => {
    var map = this.state.map;
    map.setFilter("highlightHuyen", ["==", "district_id", ""]);
    var popup = this.state.popup;
    // if (popup) popup.remove();
  }
  onXaMouseMove = (e) => {
    let me = this;
    var map = this.state.map;
    var popup = this.state.popup;
    var feature = e.features[0];
    me.setState({ xaEvent: 1 });
    setTimeout(() => {
      me.setState({ xaEvent: 0 });//bo su kien de co the click vao huyen
    }, 500);

    map.setFilter("highlightXa", ["==", "ward_id", feature.properties.ward_id]);
    if (document.getElementById('txtAddress')) document.getElementById('txtAddress').innerText = feature.properties.ward_name;
    var dmap;
    if (me.state.type === 'sale_house') {
      if (me.state.whouseObj)
        dmap = me.state.whouseObj.dmap;
    } else {
      if (me.state.wapparmentObj)
        dmap = me.state.wapparmentObj.dmap;
    }
    var props = feature.properties;

    if (dmap && dmap[props.ward_id]) {
      var info = dmap[props.ward_id];
      me.setState({ name: info.name, ppm2_mean: info.ppm2_mean.toFixed(2), minPrice: info.min.toFixed(2), maxPrice: info.max.toFixed(2) })
    }
    //   var center = polygonCenter(feature.geometry);
    //   var html = `<div>
    //   <ul style="list-style:none;">  
    //     <li><span style="font-weight:bold;">Tên: </span><span>`+ info.name + `</span></li>
    //     <li><span>Giá/m²: </span><span>`+ info.ppm2_mean.toFixed(2) + `</span></li>
    //     <li><span>Từ: </span><span>`+ info.min.toFixed(2) + `</span></li>
    //     <li><span>Đến: </span><span>`+ info.max.toFixed(2) + `</span></li>
    //   </ul>
    // </div>`
    //   popup
    //     .setLngLat(center.coordinates)
    //     .setHTML(html)
    //     .addTo(map);


  }
  renderPointFeatures = (elements) => {
    let me = this;
    var map = this.state.map;
    var features = [];
    var index = 1;
    elements.forEach(element => {
      features.push({
        'type': 'Feature',
        'id': index,
        'geometry': {
          'type': 'Point',
          'coordinates': [element.longitude, element.latitude]
        },
        'properties': {
          'id': element.id,
          'images': element.images,
          'url': element.url,
          'title': element.title,
          'address': element.address,
          'size': element.size,
          'price': element.price,
          'price_per_m2': element.price_per_m2
        }
      })
      index++;
    });
    var geodata = { 'type': 'FeatureCollection', 'features': features }
    map.getSource('points').setData(geodata);
    map.setLayoutProperty("points", 'visibility', 'visible');
  }
  stylePointFeatures = (feature) => {
    let me = this;
    var map = this.state.map;
    var promises = [];
    var houseRequest = new Promise((resolve, reject) => {
      houseApi.getByWard(feature.properties.ward_id, 1, 5000).then(res => {
        if (res && res.data.length) {
          resolve(res.data)
        } else resolve([]);
      })
    });
    promises.push(houseRequest);
    var appartmentRequest = new Promise((resolve, reject) => {
      apartmentApi.getByWard(feature.properties.ward_id, 1, 5000).then(function (response) {
        if (response && response.data.length) {
          resolve(response.data)
        } else resolve([]);
        resolve([]);
      })
    })
    promises.push(appartmentRequest);
    Promise.all(promises).then(values => {
      var houses = values[0];
      var appartments = values[1];
      me.setState({ houses: houses, appartments: appartments })
      if (me.state.type === 'sale_house') {
        this.renderPointFeatures(houses)
      }
      else
        this.renderPointFeatures(appartments)
    });
  }
  onXaSelect = (e) => {
    let me = this;
    var map = this.state.map;
    me.setState({ xaEvent: 1 });
    setTimeout(() => {
      me.setState({ xaEvent: 0 });//bo su kien de co the click vao huyen
    }, 500);
    e.preventDefault();

    var features = map.queryRenderedFeatures(e.point, { layers: ['xa'] });
    if (features && features.length > 0) {
      var feature = features[0];
      map.setFilter("selectHuyen", ["==", "city_id", ""]);
      map.setFilter("selectXa", ["==", "ward_id", feature.properties.ward_id]);
    }
  }
  onXaMouseDown = (e) => {
    let me = this;
    var map = this.state.map;
    me.setState({ xaEvent: 1 });
    setTimeout(() => {
      me.setState({ xaEvent: 0 });//bo su kien de co the click vao huyen
    }, 500);
    e.preventDefault();
    // if (me.state.tinhEvent !== 1 && me.state.huyenEvent !== 1) {
    var features = map.queryRenderedFeatures(e.point, { layers: ['xa'] });
    if (features && features.length > 0) {
      for (var i = 0; i < features.length; i++) {
        var feature = features[i];
        // console.log('id:' + feature.properties.ward_id);
        if (me.state.selectedXaId && me.state.selectedXaId === feature.properties.ward_id) {
          map.zoomIn();
        } else {
          this.onXaSelectFeature(feature, true);
        }
        break;
      }
    }
  }
  onXaSelectFeature = (feature, level) => {
    let me = this;
    var map = this.state.map;
    me.setState({ xaEvent: 1 });
    setTimeout(() => {
      me.setState({ xaEvent: 0 });//bo su kien de co the click vao huyen
    }, 500);
    var center = polygonCenter(feature.geometry);
    if (level)
      map.flyTo({ center: center.coordinates, zoom: 13.5 });
    me.stylePointFeatures(feature);

    if (me.props.wardChange) {
      let id = feature.properties.ward_id;
      me.setState({ selectedFeature: feature, selectedXaId: id })
      let name = feature.properties.ward_type + ',' + feature.properties.ward_name;
      if (!me.state.whouseObj && !me.state.wapparmentObj) return false;
      var houseObj = me.state.whouseObj.dmap[feature.properties.ward_id];
      var apparmentObj = me.state.wapparmentObj.dmap[feature.properties.ward_id];
      if (houseObj && apparmentObj) {
        let changeObj = {
          id: id,
          name: name,
          fromValueCC: apparmentObj.min.toFixed(2),
          toValueCC: apparmentObj.max.toFixed(2),
          totalMeanCC: apparmentObj.ppm2_mean.toFixed(2),
          fromValueND: houseObj.min.toFixed(2),
          toValueND: houseObj.max.toFixed(2),
          totalMeanND: houseObj.ppm2_mean.toFixed(2)
        }
        me.props.wardChange(changeObj);
      }
    }
  }
  onXaMouseLeave = (e) => {
    var map = this.state.map;
    map.getCanvas().style.cursor = '';
    map.setFilter("highlightXa", ["==", "ward_id", ""]);
  }
  onPointsMouseMove = (e) => {
    var map = this.state.map;
    var popup = this.state.popup;
    map.getCanvas().style.cursor = 'pointer';
    if (e.features.length > 0) {
      if (this.state.hoveredStateId) {
        map.setFeatureState({ source: 'points', id: this.state.hoveredStateId }, { hover: false });
      }
      this.setState({ hoveredStateId: e.features[0].id })
      map.setFeatureState({ source: 'points', id: e.features[0].id }, { hover: true });
      var coordinates = e.features[0].geometry.coordinates.slice();
      var props = e.features[0].properties;
      if (props && !props.cluster) {
        var imgsrc = '';
        if (props.images && props.images.length > 0) imgsrc = props.images.split(',')[0];
        var price_per_m2 = '';
        if (props.price_per_m2) price_per_m2 = props.price_per_m2.toFixed(2) + ' triệu';
        var html = `<div><div style="float:left;width:30%;">
                    <img style="height:100px;width:100px;" src="`+ imgsrc + `"></img>
                    <a  style="margin-left:35px;" target="_blank" href="`+ props.url + `">chi tiết</a>
                </div>
                <div style="float:left;width:70%;">
                  <ul>  
                    <li><span style="font-weight:bold;">Giá: </span><span>`+ props.price + `</span></li>
                    <li><span>Giá/m²: </span><span>`+ price_per_m2 + `</span></li>
                    <li><span>Diện tích: </span><span>`+ props.size + `</span></li>
                    <li><span>Địa chỉ: </span><span>`+ props.address + `</span></li>
                  </ul>
                </div></div>`
        popup
          .setLngLat(coordinates)
          .setHTML(html)
          .addTo(map);
      }
    }
  }
  onPointsMouseLeave = (e) => {
    var map = this.state.map;
    var popup = this.state.popup;
    if (this.state.hoveredStateId) {
      map.setFeatureState(
        { source: 'points', id: this.state.hoveredStateId },
        { hover: false }
      );
      // popup.remove();
    }
  }
  render() {
    return (
      <div>
        {this.state.showOverLay && (
          <div>
            <div className='vector-map__top-toolbar'>
              <div className="vector-map__toolbar-item vector-map__toolbar-item--tiles">
                <span >
                  Giá mỗi m2</span>
              </div>
              <div style={{ display: this.state.showOverLay }} className="vector-map__toolbar-item vector-map__toolbar-item--legend">
                <ul className="colorscale" style={{ marginLeft: "-75px" }}>
                  <li style={{ lineHeight: "0.7" }}>{this.state.minValue}</li>
                  {this.state.colors.map((item, index) => <li key={index} title={"giá từ: " + item.from + " đến " + item.to} style={{ background: item.color }} key={item.color}></li>)}
                  <li style={{ lineHeight: "0.7" }}>{this.state.maxValue}</li>
                </ul>
              </div>

            </div>
            <div className="hoverInfo" >
              <ul style={{ listStyle: "none" }} >
                <li><span style={{ fontWeight: "bold" }}>Tên: </span><span>{this.state.name}</span></li>
                <li><span >Giá/m²: </span><span>{this.state.ppm2_mean}</span></li>
                <li><span >Từ: </span><span>{this.state.minPrice}</span></li>
                <li><span >Đến: </span><span>{this.state.maxPrice}</span></li>
              </ul>
            </div>
          </div>
        )}
        <div
          ref={el => (this.mapContainer = el)}
          className="mapContainer"
          style={{ border: "solid 1px", height: "85vh" }}
        >
        </div>
      </div >
    );
  }
}
export default MapComponent;
