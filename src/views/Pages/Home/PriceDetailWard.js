import React from 'react';
import PriceDetail from './PriceDetail';

function PriceDetailWard(props){
    return(
        <PriceDetail {...props} bdsType={props.bdsType} />
    )
}
export default PriceDetailWard;