import React, { useEffect } from 'react';
import './Price.css';
import { Row, Col, Button, ButtonGroup, Table, Card, CardBody, CardFooter } from 'reactstrap';
import { Line } from 'react-chartjs-2';
import mapApi from './../../../api/map.api';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import houseApi from '../../../api/house.api';
import apartmentApi from '../../../api/appartment.api';

const datasetDefault = {
    label: 'Price',
    backgroundColor: 'rgba(75,192,192,0.2)',
    borderColor: '#63c2de',
    pointHoverBackgroundColor: '#fff',
    borderWidth: 2,
};
const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips,
    },
    maintainAspectRatio: false,
    legend: {
        display: false,
    },
    elements: {
        point: {
            radius: 0,
            hitRadius: 10,
            hoverRadius: 4,
            hoverBorderWidth: 3,
        },
    },

}
class Price extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            areaId: '',
            areaType: '',
            bdsType: 'apartment',
            update: false,
            showChart: false,
            showModal: false,
            location: {},
            data: [],
            dataChartHouse: [],
            dataChartAppartment: [],
            chartOf: 'house',
            radioSelected: 1,
            lineChart: {
                labels: [],
                datasets: [{
                    ...datasetDefault,
                    data: [],
                },
                ]
            },
            datatableHouse: [],
            datatableApartment: [],
            datatable: [],
            tableOf: 'apartment',
            showColLeft: true,
            textSearch: '',
        }

        this.mapRef = React.createRef();
    }
    // componentWillUpdate(props) {
    //     // console.log("helloo", props)
    //     if (props !== this.props) {
    //         console.log("helloo");
    //         this.loadTable(props.typeBds);
    //     }
    // }
    onCityChange = (obj) => {
        console.log('onCity change:' + obj.id);
        // this.props.history.push('/price');
        this.setState({ areaType: 'city', areaId: obj.id });
        this.loadCityData(obj.id);
    }

    onDistrictChange = (obj) => {
        this.setState({ areaType: 'district', areaId: obj.id });
        this.loadDistrictData(obj.id);
    }
    chooseHome(value) {
        this.setState({ bdsType: value });
    }

    onWardChange = (obj) => {
        this.setState({ areaType: 'ward', areaId: obj.id });
        this.loadWardData(obj.id);

    }
    changeBdsType(val) {
        this.loadTable(val);
    }

    loadTable(type) {
        let idLocation = this.state.areaId;
        console.log('load table:' + idLocation);
        if (type === 'house') {
            mapApi.getDistrictHouseStyle(idLocation).then(data => {
                console.log('house data:');
                console.log(data);
                this.setState({ datatable: data, bdsType: type });

            })
        }
        else {
            mapApi.getDistrictAppartmentStyle(idLocation).then(data => {
                console.log('apartment data:');
                console.log(data);
                this.setState({ datatable: data, bdsType: type });
            })
        }
    }

    onRadioBtnClick = (radioSelected) => {
        this.loadChart(this.state.bdsType, radioSelected);
    }
    loadCityData = (id) => {
        var promises = [];
        var dhouseStyle = new Promise((resolve, reject) => {
            mapApi.getDistrictHouseStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(dhouseStyle);
        var dapartmentstyle = new Promise((resolve, reject) => {
            mapApi.getDistrictAppartmentStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(dapartmentstyle);

        Promise.all(promises).then(values => {
            this.setState({
                dataChartHouse: values[0],
                dataChartAppartment: values[1]
            });
            this.loadChart(this.state.bdsType, this.state.radioSelected);
            this.loadTable(this.state.bdsType);
        })
    }

    loadDistrictData = (id) => {
        let me = this;
        var promises = [];
        var getPriceHouse = new Promise((resolve, reject) => {
            houseApi.getPriceByDistrict(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(getPriceHouse);
        var getPirceApartment = new Promise((resolve, reject) => {
            apartmentApi.getPriceByDistrict(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getPirceApartment);
        var getWardHouse = new Promise((resolve, reject) => {
            mapApi.getWardHouseStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getWardHouse);
        var getWardApartment = new Promise((resolve, reject) => {
            mapApi.getWardAppartmentStyle(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getWardApartment);
        Promise.all(promises).then(values => {
            this.setState({
                dataChartHouse: values[0],
                dataChartAppartment: values[1],
                datatableHouse: values[2],
                datatableApartment: values[3],
                id: id
            });
            this.loadChart(this.state.bdsType, this.state.radioSelected);
            this.loadTable(this.state.bdsType);
        })
    }
    loadWardData = (id) => {
        var promises = [];
        var getPriceHouse = new Promise((resolve, reject) => {
            houseApi.getPriceByWard(id).then(function (response) {
                if (response && response.length) {
                    resolve(response)
                } else
                    resolve([]);
            })
        })
        promises.push(getPriceHouse);
        var getPirceApartment = new Promise((resolve, reject) => {
            apartmentApi.getPriceByWard(id).then(function (response) {
                if (response && response.length) {
                    resolve(response);
                }
                else
                    resolve([]);
            })
        })
        promises.push(getPirceApartment);

        Promise.all(promises).then(values => {
            this.setState({
                dataChartHouse: values[0],
                dataChartAppartment: values[1],
                datatableHouse: [],
                datatableApartment: [],
                datatable: [],
                id: id
            });
            this.loadChart(this.state.bdsType, this.state.radioSelected);
        })
    }
    loadChart = (chartOf, radioSelected) => {
        let data;
        if (chartOf === 'house') data = this.state.dataChartHouse;
        else data = this.state.dataChartAppartment;
        if (data && data.length > 0) {
            let temp;
            let arrLabel = [];
            let arrData = [];
            switch (radioSelected) {
                case 1: {
                    temp = data[0].price_filter_by_time.day;
                    for (var i of temp) {
                        arrLabel.push(i.date_str);
                        arrData.push(i.mean_ppm2.toFixed(1));
                    }
                    break;
                }
                case 2: {
                    temp = data[0].price_filter_by_time.week;
                    for (var i of temp) {
                        arrLabel.push(i.week_in_year.toString());
                        arrData.push(i.mean_ppm2.toFixed(1));
                    }
                    break;
                }
                case 3: {
                    temp = data[0].price_filter_by_time.month;
                    for (var i of temp) {
                        arrLabel.push(i.month_str);
                        arrData.push(i.mean_ppm2.toFixed(1));
                    }
                    break;
                }
                default: {
                    temp = data[0].price_filter_by_time.year;
                    for (var i of temp) {
                        arrLabel.push(i.year_str);
                        arrData.push(i.mean_ppm2.toFixed(1));
                    }
                }
            }

            let datasetTemp = {
                ...datasetDefault,
                data: arrData,
            }
            this.setState(prevState => ({
                lineChart: {
                    ...prevState.lineChart,
                    labels: arrLabel,
                    datasets: [datasetTemp],
                },
                radioSelected: radioSelected,
                bdsType: chartOf,
            }));
        }
    }
    render() {
        return (
            <div>
                <Card >
                    <PerfectScrollbar>
                        <CardBody style={{ height: "50vh" }} >
                            <div>
                                <Row>
                                    <Col className='text-right'>
                                        <div>
                                            <ButtonGroup className="mr-3" aria-label="First group">
                                                <Button color="outline-primary"
                                                    onClick={() => this.onRadioBtnClick(1)}
                                                    active={this.state.radioSelected === 1}
                                                >
                                                    Ngày
                                                                </Button>
                                                <Button color="outline-primary"
                                                    onClick={() => this.onRadioBtnClick(2)}
                                                    active={this.state.radioSelected === 2}
                                                >
                                                    Tuần
                                                                </Button>
                                                <Button color="outline-primary"
                                                    onClick={() => this.onRadioBtnClick(3)}
                                                    active={this.state.radioSelected === 3}
                                                >
                                                    Tháng
                                                                </Button>
                                                <Button color="outline-primary"
                                                    onClick={() => this.onRadioBtnClick(4)}
                                                    active={this.state.radioSelected === 4}
                                                >
                                                    Năm
                                                                </Button>
                                            </ButtonGroup>

                                        </div>
                                    </Col>
                                </Row>
                                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                                    <Line data={this.state.lineChart} options={options} height={300} />
                                </div>
                            </div>
                            <div>
                                <Table>
                                    <thead>
                                        <tr>
                                            <th>Khu vực</th>
                                            <th>Giá thấp nhất</th>
                                            <th>Giá cao nhất</th>
                                            <th>Giá trung bình</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        {/* <PerfectScrollbar> */}
                                        {this.state.datatable.map(i => (
                                            <tr key={i.id}>
                                                <td>{i.name}</td>
                                                <td>{i.min.toFixed(2)}</td>
                                                <td>{i.max.toFixed(2)}</td>
                                                <td>{i.ppm2_mean.toFixed(2)}</td>
                                            </tr>
                                        ))}
                                        {/* </PerfectScrollbar> */}
                                    </tbody>
                                </Table>
                            </div>
                        </CardBody>
                    </PerfectScrollbar>
                    <CardFooter style={{ padding: " 0", float: "right", height: "34px" }}>
                        {/* <div id="react-paginate" style={{ paddingTop: "0px" }} className={isView ? "" : "d-none"} >
                            <ReactPaginate
                                previousLabel={'Trước'}
                                nextLabel={'Tiếp'}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={3}
                                onPageChange={this.handlePageChange}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}
                                forcePage={this.state.forcePage}
                            />
                        </div> */}
                    </CardFooter>
                </Card>
            </div>
        )
    }
}

export default Price;